<?php
/**
 * @file
 * Lets users relate nodes.
 *
 * Adds search fields when a node is edited
 * so that authenticated users may relate nodes.
 */
/**
 * Implementation of hook_perm().
 */
function relateditems_perm() {
  $perm = array();
  $perm[] = "view related";
  $perm[] = "create related";
  $perm[] = "delete related";
  $perm[] = "delete own related";
  return $perm;
}
function relateditems_prepare_access() {
  if (
    user_access("administer") ||
    user_access("create related") ||
    user_access("delete related") ||
    user_access("delete own related")
  ) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}
/**
 * Implementation of hook_menu().
 */
function relateditems_menu() {
  $items = array();

  $items['admin/settings/relateditems'] = array(
    'title' => t('Related Items'),
    'description' => t('Select what node types Related Items will affect, what node types you can relate and more.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('relateditems_admin_settings'),
    'access arguments' => array('administer site configuration'),
  );

  $items['relateditems/autocomplete'] = array(
    'title' => 'relateditems-'. time(),
    'page callback' => 'relateditems_autocomplete',
    'access callback' => relateditems_prepare_access(),
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK
  );
  $items['relateditems/op'] = array(
    'title' => 'relateditems-op-'. time(),
    'page callback' => 'relateditems_op',
    'access callback' => relateditems_prepare_access(),
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK
  );

  return $items;
}
/**
 * Define the settings form.
 */
function relateditems_admin_settings() {
  $form['relateditems_enable_nodetypes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enable Related Items for'),
    '#options' => node_get_types('names'),
    '#default_value' => variable_get('relateditems_enable_nodetypes', array('story')),
    '#description' => t('Select on what node types this module will be enabled.')
  );
  $form['relateditems_relate_nodetypes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Relate nodes from'),
    '#options' => node_get_types('names'),
    '#default_value' => variable_get('relateditems_relate_nodetypes', array('story')),
    '#description' => t('Select what node types can be related.')
  );
  $form['relateditems_query_limit'] = array(
    '#type' => 'select',
    '#title' => t('Search query limit'),
    '#options' => array('10' => 10, '20' => 20, '30' => 30, '40' => 40, '50' => 50),
    '#default_value' => variable_get('relateditems_query_limit', array('20' => 20)),
    '#description' => t('Set how many results will the query/search return.')
  );
  $form['relateditems_watchdog_append'] = array(
    '#type' => 'checkbox',
    '#title' => t('Append to watchdog'),
    '#default_value' => variable_get('relateditems_watchdog_append', 0),
    '#description' => t('Check this for logging Related Items operations.')
  );
  $form['relateditems_show_nodetype'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show node types'),
    '#default_value' => variable_get('relateditems_show_nodetype', 1),
    '#description' => t('Check this if you want to list the relate items by node types, in the dedicated block.')
  );
  $form['relateditems_block_type'] = array(
    '#type' => 'checkbox',
    '#title' => t('Block type: Table'),
    '#default_value' => variable_get('relateditems_block_type', 1),
    '#description' => t('Check this if you want to list the relate items in the block by table style, otherwise the style is list.')
  );
  $form['array_filter'] = array('#type' => 'hidden');
  return system_settings_form($form);
}
/**
 * Implementation of hook_nodeapi().
 */
function relateditems_nodeapi(&$node, $op, &$a3, $a4) {
  global $user;
  // return if the user has no access roles
  if (
    !user_access("view related") &&
    !user_access("create related") &&
    !user_access("delete related") &&
    !user_access("delete own related")
  ) {
    return;
  }
  // return if this node type is not defined in settings:
  if (!in_array($node->type, variable_get('relateditems_enable_nodetypes', array('story')))) {
    return;
  }
  // ops:
  switch ($op) {
    // ---- LOAD:
    case 'load':
      // brake if the user has no access roles
      if (
        !user_access("view related") &&
        !user_access("create related") &&
        !user_access("delete related") &&
        !user_access("delete own related")
      ) {
        break;
      }
      // prepare view array:
      $a_view = array();
      $a_edit = array();
      $o_relate_to_query = db_query("SELECT ri.* , n.title, n.type, n.status, nt.name as type_name FROM {relateditems} AS ri, {node} AS n, {node_type} AS nt WHERE n.nid = ri.did AND nt.type = n.type AND ri.sid = %d order by timestamp asc", $node->nid);
      $o_relate_from_query = db_query("SELECT ri.* , n.title, n.type, n.status, nt.name as type_name FROM {relateditems} AS ri, {node} AS n, {node_type} AS nt WHERE n.nid = ri.sid AND nt.type = n.type AND ri.did = %d order by timestamp asc", $node->nid);
      while ($a_related_to = db_fetch_array($o_relate_to_query)) {
        // view:
        if (!isset($a_view[$a_related_to['did']])) {
          $a_view[$a_related_to['did']] = $a_related_to;
        }
        // edit:
        if ( user_access("delete related") || user_access("delete own related") ) {
          $a_this_edit = array();
          if (user_access("delete own related")) {
            if ($a_related_to['uid'] == $user->uid) {
              $a_this_edit = $a_related_to;
            }
          }
          if (user_access("delete related")) {
            $a_this_edit = $a_related_to;
          }
          if (!empty($a_this_edit)) {
            $a_edit[] = $a_this_edit;
          }
        }
      }
      while ($a_related_from = db_fetch_array($o_relate_from_query)) {
        if (!isset($a_view[$a_related_from['sid']])) {
          $a_view[$a_related_from['sid']] = $a_related_from;
        }
        // edit:
        if ( user_access("delete related") || user_access("delete own related") ) {
          $a_this_edit = array();
          if (user_access("delete own related")) {
            if ($a_related_from['uid'] == $user->uid) {
              $a_this_edit = $a_related_from;
            }
          }
          if (user_access("delete related")) {
            $a_this_edit = $a_related_from;
          }
          if (!empty($a_this_edit)) {
            $a_edit[] = $a_this_edit;
          }
        }
      }
      $a_temp_view = $a_view;
      $a_view = array();
      foreach ($a_temp_view as $i_key => $a_value) {
        if ((int)$a_value['status'] == 1) {
          if (isset($a_view[$a_value['type']])) {
            $a_view[$a_value['type']]['relateds'][] = array('nid' => $i_key, 'title' => $a_value['title']);
          }
          else {
            $a_view[$a_value['type']] = array();
            $a_view[$a_value['type']]['type_name'] = $a_value['type_name'];
            $a_view[$a_value['type']]['relateds'] = array();
            $a_view[$a_value['type']]['relateds'][] = array('nid' => $i_key, 'title' => $a_value['title']);
          }
        }
      }
      // add to node:
      $node->relateditems = array('view' => $a_view, 'edit' => $a_edit);
      break;

    // ---- VIEW:
    case 'view':
      // moved to the dedicated block ...
      break;
  }
}
/**
 * Implementation of hook_form_alter().
 */
function relateditems_form_alter(&$form, &$form_state, $form_id) {
  if (
    !user_access("create related") &&
    !user_access("delete related") &&
    !user_access("delete own related")
  ) {
    return;
  }
  if (!strpos($form['#action'], "node/add")) {
    $form['relateditems'] = relateditems_add_edit_form($form);
  }
  else {
    return;
  }
}
/**
 * Define the form for adding/editing related items
 */
function relateditems_add_edit_form($o_form) {
  $node = $o_form['#node'];
  if (isset($node->relateditems)) {
    // add the css:
    drupal_add_css(drupal_get_path('module', 'relateditems') ."/relateditems.css");
    // add the js:
    drupal_add_js('var riBasePath = "'. base_path() .'";', 'inline', 'header', FALSE, FALSE);
    drupal_add_js(drupal_get_path('module', 'relateditems') ."/relateditems.js");
    // define alowed node types titles:
    $alowed_node_types = array();
    $first_element = NULL;
    foreach (node_get_types('names') as $node_type => $node_type_title) {
      if (in_array($node_type, variable_get('relateditems_relate_nodetypes', array('story')))) {
        $alowed_node_types[$node_type] = $node_type_title;
        if (!$first_element) {
          $first_element = $node_type;
        }
      }
    }
    if (!$first_element) {
      $first_element = 'story';
    }
    //prepare default value for related items:
    $ri_form = array(
      '#type' => 'fieldset',
      '#title' => t('Related Items'),
      '#collapsible' => TRUE,
      '#prefix' => '',
      '#suffix' => '',
      '#weight' => 100,
    );
    //
    $ri_form['relateditems_notify'] = array(
      '#type' => 'markup',
      '#value' => '&nbsp;',
      '#prefix' => '<div id="relateditems_notify" align="center" style="display:none;">',
      '#suffix' => '</div>',
    );
    //
    $ri_form['relateditems_container_div_open'] = array(
      '#type' => 'markup',
      '#value' => '<div id="relateditems_container_div" class="relateditems_container_css">',
    );
    //
    $s_relateds_list = ' ';
    foreach ($node->relateditems['edit'] as $related) {
      //
      $s_related_table  = '';
      $s_related_table .= '<tr id="relateditems_ri_container-'. $related['did'] .'">';
      $s_related_table .= '<td align="right" valign="middle">';
      $s_related_table .= $related['type_name'];
      $s_related_table .= '</td>';
      $s_related_table .= '<td align="left" valign="middle">';
      $s_related_table .= l($related['title'], 'node/'. $related['did'], array('attributes' => array('target' => '_self'), 'absolute' => TRUE, 'html' => TRUE));
      $s_related_table .= '</td>';
      $s_related_table .= '<td align="center" valign="middle">';
      $s_related_table .= '<a href="javascript:relateditems_op(\'delete\', \''. $related['sid'] .'\', \''. $related['did'] .'\', \'\');" onclick="return confirm(\''. t('Are you sure ?') .'\');">delete</a>';
      $s_related_table .= '</td>';
      $s_related_table .= '</tr>';
      $s_related_table .= '';
      //
      $s_relateds_list .= $s_related_table;
    }
    $ri_form['relateditems_ri_container_list'] = array(
      '#type' => 'markup',
      '#value' => $s_relateds_list,
      '#prefix' => '<div><table id="relateditems_ri_container_list" cellpadding="0" cellspacing="0" border="0">',
      '#suffix' => '</table></div>',
    );
    $ri_form['relateditems_ri_new_container'] = array(
      '#type' => 'markup',
      '#value' => '&nbsp;',
      '#prefix' => '<div id="relateditems_ri_new_container">',
      '#suffix' => '</div>',
    );
    // only if access it's TRUE,
    // hidden fields for js(dynamic add):
    if (user_access("create related")) {
      $ri_form['fieldset_group_'."jsr3pl4c3"] = array(
        '#type' => 'fieldset',
        '#title' => '<b>'.'New related'.'</b>',
        '#collapsible' => FALSE,
        '#prefix' => '<div id="relateditems_hidden_fields_js" style="display:none;">',
        '#suffix' => '</div>',
      );
      $ri_form['fieldset_group_'."jsr3pl4c3"]['relateditems_alowed_nodetypes-'."jsr3pl4c3"] = array(
        '#type' => 'select',
        '#default_value' => array($first_element),
        '#options' => $alowed_node_types,
        '#attributes' => array('onchange' => "relateditems_nodetype('". base_path() ."', 'jsr3pl4c3', '". $node->nid ."')"),
        '#prefix' => '',
        '#suffix' => '',
      );
      //
      $ri_form['fieldset_group_'."jsr3pl4c3"]['relateditems_node_title_search-'."jsr3pl4c3"] = array(
        '#type' => 'textfield',
        '#size' => 60,
        '#maxlength' => 255,
        '#default_value' => "",
        '#autocomplete_path' => 'relateditems/autocomplete/'. time() .'/'. $first_element .'/'. $node->nid,
        '#attributes' => array('style' => 'width:550px;'),
        '#prefix' => '',
        '#suffix' => '<a href="javascript:relateditems_op(\'create\', \''. $node->nid .'\', \'jsr3pl4c3\', \'\');">SAVE</a><br/><a href="javascript:relateditems_remove(\'relateditems_remove_container-'."jsr3pl4c3".'\');" onclick="return confirm(\''. t('Are you sure ?') .'\');">'. t('Delete this related') .'</a>',
      );
      //the "add" link:
      $ri_form['add_related_link'] = array(
        '#type' => 'markup',
        '#value' => '<b><a href="javascript:relateditems_add();">'. t('Add related') .'</a></b>',
      );
    }
    $ri_form['relateditems_container_div_close'] = array(
      '#type' => 'markup',
      '#value' => '</div>',
    );
    return $ri_form;
  }
}
/**
 * Define relateditems autocomplete function
 */
function relateditems_autocomplete($nocache=0, $node_type='story', $ex_node_id=0, $node_title='') {
  $a_results = array();
  $query_limit = (int)variable_get('relateditems_query_limit', array('20' => 20));
  $search_query = db_query_range(db_rewrite_sql("SELECT n.nid, n.title FROM {node} n WHERE n.title LIKE '%%%s%' AND n.type = '%s' AND n.nid <> %d AND n.status = %d ORDER BY n.title", 'node.nid'), $node_title, $node_type, $ex_node_id, 1, 0, $query_limit);
  while ($o_search = db_fetch_object($search_query)) {
    // eliminate existing relateds:
    $check_query = db_query_range("SELECT uid FROM {relateditems} WHERE sid=%d AND did=%d", $ex_node_id, $o_search->nid, 0, 1);
    if (!db_result($check_query)) {
      $a_results[$o_search->title ." [nid:". $o_search->nid ."]"] = check_plain($o_search->title);
    }
  }
  print drupal_to_js($a_results);
}
/**
 * Define relateditems operations function
 */
function relateditems_op($op='', $sid=0, $did=0) {
  $sid = (int)$sid;
  $did = (int)$did;
  if ($sid == 0 || $did == 0) {
    $a_return['status'] = 0;
    $a_return['feedback'] = t('Error. One or both the nids equal 0. (sid=@sid,did=@did)', array('@sid'  => $sid, '@did'  => $did));
  }
  else {
    global $user;
    $a_return = array();
    switch ($op) {
      case 'create':
      // check permission:
      if (user_access("create related")) {
        // check existing relateds:
        $check_query = db_query_range("SELECT uid FROM {relateditems} WHERE sid=%d AND did=%d", $sid, $did, 0, 1);
        if (!db_result($check_query)) {
          $new_related = db_query("INSERT INTO {relateditems} (sid, did, uid, timestamp) VALUES (%d, %d, %d, %d)", $sid, $did, $user->uid, time());
          if ($new_related) {
            $node = node_load($did);
            $a_return['node_path'] = url("node/". $did, array('absolute' => TRUE));
            $a_return['title'] = $node->title;
            $a_return['type_name'] = node_get_types('name', $node->type);
            $a_return['status'] = 1;
            $a_return['feedback'] = t("Related item succesfully added.");
            // handle watchdog:
            if (variable_get("relateditems_watchdog_append", 0)) {
              $a_t = array(
              '@sid' => $sid,
              '@sid_title' => node_load($sid)->title,
              '@did' => $did,
              '@did_title'  => $a_return['title']
              );
              $s_watchdog_message = t('Added node @did[@did_title] as related item to node @sid[@sid_title]', $a_t);
              watchdog("relateditems", $s_watchdog_message);
            }
          }
          else {
            $a_return['status'] = 0;
            $a_return['feedback'] = t("DB error. Details:") ."<br/><br/>". db_error();
            // handle watchdog:
            if (variable_get("relateditems_watchdog_append", 0)) {
              watchdog("relateditems", db_error(), array(), WATCHDOG_ERROR);
            }
          }
        }
        else {
          $a_return['status'] = 0;
          $a_return['feedback'] = t("This node is already related.");
        }
      }
      else {
        $a_return['status'] = 0;
        $a_return['feedback'] = t('You do not have permission to RELATE nodes.!');
      }
      //
      break;
      //
      case 'delete':
      // get user's id:
      $user_id_query = db_query("SELECT uid FROM {relateditems} WHERE sid=%d AND did=%d", $sid, $did);
      $ri_uid = db_result($user_id_query);
      if ($ri_uid) {
        // handle perms cases:
        $b_status = FALSE;
        switch (TRUE) {
          case user_access("delete related"):
          $delete_query = db_query("DELETE FROM {relateditems} WHERE sid=%d AND did=%d", $sid, $did);
          if ($delete_query) {
            $b_status = TRUE;
            $a_return['feedback'] = t('Related item succesfully deleted.');
          }
          else {
            $b_status = FALSE;
            $a_return['feedback'] = t("DB error. Details:") ."<br/><br/>". db_error();
            // handle watchdog:
            if (variable_get("relateditems_watchdog_append", 0)) {
              watchdog("relateditems", db_error(), array(), WATCHDOG_ERROR);
            }
          }
          break;
          case user_access("delete own related"):
          if ($ri_uid == $user->uid) {
            $delete_query = db_query("DELETE FROM {relateditems} WHERE sid=%d AND did=%d AND uid=%d", $sid, $did, $user->uid);
            if ($delete_query) {
              $b_status = TRUE;
              $a_return['feedback'] = t('Related item succesfully deleted.');
            }
            else {
              $b_status = FALSE;
              $a_return['feedback'] = t("DB error. Details:") ."<br/><br/>". db_error();
              // handle watchdog:
              if (variable_get("relateditems_watchdog_append", 0)) {
                watchdog("relateditems", db_error(), array(), WATCHDOG_ERROR);
              }
            }
          }
          else {
            $b_status = FALSE;
            $a_return['feedback'] = t('You do not have permission to DELETE this related. It was not created by you. This action was logged.');
            // handle watchdog:
            $a_t = array(
              '@sid' => $sid,
              '@sid_title' => node_load($sid)->title,
              '@did' => $did,
              '@did_title' => node_load($did)->title
            );
            $s_watchdog_message = t('User tried to unreleate(delete) @did[@did_title] from node @sid[@sid_title] without having appropiate permission. Possibly a hack.', $a_t);
            if (variable_get("relateditems_watchdog_append", 0)) {
              watchdog("relateditems", $s_watchdog_message, array(), WATCHDOG_WARNING);
            }
          }
          break;
          case(user_access("delete related") && user_access("delete own related")):
          $delete_query = db_query("DELETE FROM {relateditems} WHERE sid=%d AND did=%d", $sid, $did);
          if ($delete_query) {
            $b_status = TRUE;
            $a_return['feedback'] = t('Related item succesfully deleted.');
          }
          else {
            $b_status = FALSE;
            $a_return['feedback'] = t("DB error. Details:") ."<br/><br/>". db_error();
            // handle watchdog:
            if (variable_get("relateditems_watchdog_append", 0)) {
              watchdog("relateditems", db_error(), array(), WATCHDOG_ERROR);
            }
          }
          break;
        }
        if ($b_status) {
          $a_return['status'] = 1;
          // handle watchdog:
          if (variable_get("relateditems_watchdog_append", 0)) {
            $a_t = array(
              '@sid' => $sid,
              '@sid_title' => node_load($sid)->title,
              '@did' => $did,
              '@did_title' => node_load($did)->title
            );
            $s_watchdog_message = t('Node @did[@did_title] was deleted as related item from node @sid[@sid_title]', $a_t);
            watchdog("relateditems", array(), $s_watchdog_message);
          }
        }
        else {
          $a_return['status'] = 0;
        }
      }
      else {
        $a_return['status'] = 0;
        $a_return['feedback'] = t('This related does not exist.');
      }
      break;
    }
  }
  $a_return['op'] = $op;
  $a_return['sid'] = $sid;
  $a_return['did'] = $did;
  // print json:
  print drupal_to_js($a_return);
  exit();
}
/**
 * Implementation of hook_block().
 */
function relateditems_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
    case 'list':
      $info[0]['info'] = t('Related Items');
      return $info;
      break;
    case 'view':
      if (user_access("view related")) {
        if ($node = menu_get_object()) {
          // generate the content:
          if ($node->relateditems['view']) {
            $view_build_array = $node->relateditems['view'];

            $header = array();
            $rows = array();
            $output = '';

            if (!empty($view_build_array)) {
              foreach ($view_build_array as $node_type) {
                $related_table = array();
                //print node type name:
                if (variable_get('relateditems_show_nodetype', 1)==1) {
                  $related_table[] = $node_type['type_name'];
                  $output .= $node_type['type_name'];
                }
                $related_table[] = '';
                $rows[] = $related_table;

                $items = array();
                //print relateds from this node type:
                foreach ($node_type['relateds'] as $related) {
                  $related_table = array();
                  if (variable_get('relateditems_show_nodetype', 1)==1) {
                    $related_table[] = '&nbsp;';
                  }
                  $related_table[] = l($related['title'], 'node/'. $related['nid'], array('attributes' => array('target' => '_self'), 'absolute' => TRUE, 'html' => TRUE));
                  $items[] = l($related['title'], 'node/'. $related['nid'], array('attributes' => array('target' => '_self'), 'absolute' => TRUE, 'html' => TRUE));
                  $rows[] = $related_table;
                }
                $output .= theme('item_list', $items, $title, 'ul');
              }
            }
            $block['subject'] = t('Related Items');
            if (variable_get('relateditems_block_type', 1)) {
              $block['content'] = theme('table', $header, $rows, array('id' => 'relateditem_list'));
            }
            else {
              $block['content'] = $output;
            }
            return $block;
          }
        }
      }
      return $block;
      break;
  }
}


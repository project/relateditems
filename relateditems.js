/**
add fields dynamically:
*/
function relateditems_add() {
	var milisecTime = new Date().getTime();
	//milisecTime = parseInt(milisecTime/1000);
	var relatedItemsContainer = document.getElementById('relateditems_ri_new_container');
	var forReplaceContainer = document.getElementById('relateditems_hidden_fields_js');
	var newRelateString = forReplaceContainer.innerHTML.replace(/jsr3pl4c3/g, milisecTime);
	var newRelatedElement = document.createElement('div');
	newRelatedElement.id = 'relateditems_remove_container-'+milisecTime;
	newRelateString = newRelateString.replace(/autocomplete-processed/g, '');
	newRelatedElement.innerHTML = newRelateString;
	relatedItemsContainer.appendChild(newRelatedElement);
	Drupal.attachBehaviors('#'+newRelatedElement.id);
	//Drupal.behaviors.autocomplete('#'+newRelatedElement.id);
}
/**
remove fields:
*/
function relateditems_remove(relatedContainerID) {
	var relatedItemsContainer = document.getElementById('relateditems_ri_new_container');
	var relateItemToRemove = document.getElementById(relatedContainerID);
	relatedItemsContainer.removeChild(relateItemToRemove);
}
/**
change node type:
*/
function relateditems_nodetype(basePath, replace, nodeID) {
	var milisecTime = new Date().getTime();
	milisecTime = parseInt(milisecTime/1000);
	var relatedItemsAutoField = document.getElementById('edit-relateditems-node-title-search-'+replace+'-autocomplete');
	//var relatedItemsNodeField = document.getElementById('edit-relateditems-alowed-nodetypes-'+replace);
	var relatedItemsNodeTypeField = document.getElementById('edit-relateditems-alowed-nodetypes-'+replace).value;
	var relatedItemsTextField = document.getElementById('edit-relateditems-node-title-search-'+replace).value;
	relatedItemsAutoField.value = basePath+'relateditems/autocomplete/'+milisecTime+'/'+relatedItemsNodeTypeField+'/'+nodeID;

  var relatedElement = document.getElementById('relateditems_remove_container-'+replace);
  relatedElement.innerHTML = relatedElement.innerHTML.replace(/autocomplete-processed/g, '');
  //Drupal.attachBehaviors('#'+relatedElement.id);
  Drupal.behaviors.autocomplete('#'+relatedElement.id);
  document.getElementById('edit-relateditems-alowed-nodetypes-'+replace).value = relatedItemsNodeTypeField;
  document.getElementById('edit-relateditems-node-title-search-'+replace).value = relatedItemsTextField;
	//var input = $('#' + 'edit-relateditems-node-title-search-'+replace);
}
/**
operations:
*/
function relateditems_op(op, sid, did, data) {
	var notifyContainer = document.getElementById('relateditems_notify');//$("#relateditems_notify");
	var relatedsListContainer = document.getElementById('relateditems_ri_container_list');//$("#relateditems_ri_container_list");
	if (data.length == 0) {
		// handle send:
		switch(op) {
			// create:
			case 'create':
			var tempDidContainer = did;
			var relatedValue = document.getElementById('edit-relateditems-node-title-search-'+did).value;
			// extract did:
			var startPos = relatedValue.lastIndexOf(":")+1;
			var endPos = relatedValue.lastIndexOf("]");
			var did = relatedValue.substring(startPos, endPos);
			// build get path:
			var path = riBasePath+'relateditems/op/create/'+sid+'/'+did;
			// make the call:
			$.get(path, null, relateditemsOpFeedback);
			// remove edit fields:
			relateditems_remove('relateditems_remove_container-'+tempDidContainer);
			break;
			// delete:
			case 'delete':
			// build get path:
			var path = riBasePath+'relateditems/op/delete/'+sid+'/'+did;
			// make the call:
			$.get(path, null, relateditemsOpFeedback);
			break;
		}
	}
	else {
		// handle receive:
		switch(op) {
			// create:
			case 'create':
			if (data['status'] == 1) {
				// alter related list:
				var newTR = relatedsListContainer.insertRow(relatedsListContainer.rows.length);
				newTR.id = 'relateditems_ri_container-'+data['did'];
				var newTDNodeType = newTR.insertCell(0);
				newTDNodeType.align = "right";
				newTDNodeType.vAlign = "middle";
				newTDNodeType.innerHTML = data['type_name'];
				var newTDNodeTitle = newTR.insertCell(1);
				newTDNodeTitle.align = "left";
				newTDNodeTitle.vAlign = "middle";
				newTDNodeTitle.innerHTML = '<a href="'+data['node_path']+'" target="_blank">'+data['title']+'</a>';
				var newTDDelete = newTR.insertCell(2);
				newTDDelete.align = "center";
				newTDDelete.vAlign = "middle";
				newTDDelete.innerHTML = '<a href="javascript:relateditems_op(\'delete\', \''+data['sid']+'\', \''+data['did']+'\', \'\');" onclick="return confirm(\'Are you sure ?\');">delete</a>';
				// alter class
				notifyContainer.className = 'relateditems_notify_green';
				// write feedback:
				notifyContainer.innerHTML = data['feedback'];
				// display it:
				notifyContainer.style.display = 'block';
				// hide it(jQuery effect):
				$("#relateditems_notify").fadeOut(5000, function (){
					notifyContainer.style.display = 'none';
				});
			}
			else {
				// alter class
				notifyContainer.className = 'relateditems_notify_red';
				// write feedback:
				notifyContainer.innerHTML = data['feedback'];
				// display it:
				notifyContainer.style.display = 'block';
			}
			//
			break;
			// delete:
			case 'delete':
			if (data['status'] == 1) {
				// remove row from table:
				for (var i=0; i<relatedsListContainer.rows.length; i++) {
					if (relatedsListContainer.rows[i].id == 'relateditems_ri_container-'+data['did']) {
						relatedsListContainer.deleteRow(i);
					}
				}
				// alter class
				notifyContainer.className = 'relateditems_notify_green';
				// write feedback:
				notifyContainer.innerHTML = data['feedback'];
				// display it:
				notifyContainer.style.display = 'block';
				// hide it(jQuery effect):
				$("#relateditems_notify").fadeOut(5000, function (){
					notifyContainer.style.display = 'none';
				});
			}
			else {
				// alter class
				notifyContainer.className = 'relateditems_notify_red';
				// write feedback:
				notifyContainer.innerHTML = data['feedback'];
				// display it:
				notifyContainer.style.display = 'block';

			}
			break;
		}
	}
}
/**
ajax feedback:
*/
var relateditemsOpFeedback = function (data) {
	var aResult = Drupal.parseJson(data);
	relateditems_op(aResult['op'], aResult['sid'], aResult['did'], aResult);
}
